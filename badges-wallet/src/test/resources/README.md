### Bienvenue dans ce cours CNAM en FOAD, NF035

Vous trouverez dans ce fichier **Readme** l'énoncé de chaque exercice noté dans le cadre du projet tutoré central à cet enseignement sur **JAVA: Bibliothèques et Patterns**.
_La solution de chaque exercice fera d'ailleurs l'objet d'un nouveau projet Git ici-même._

**Même si votre solution est bonne et recevable avec tous les points, il est par ailleurs préférable de repartir de la présente solution pour avancer dans ce projet tutoré sans risque, étant donné ce qui vous sera demandé en suite.**

Voici l'énoncé du 1er exercice noté de la 2ème session de ce projet.

---

# Listes, Ensembles et Couches logicielles
 ++ Intégration d'une couche logicielle transverse: commons/utils

## Contexte
* Au programme de ce cours: Listes, Ensembles, Couches logicielles
## Objectifs
* Mises en application:
 - [x] (Exercice 1) Homogénéisation du projet en suivant le standard Maven (Exercice 1)
 - [ ] Permettre au wallet de recevoir plusieurs badges, et de les restituer sous forme de liste
    - [ ] (Exercice 2) Ecriture séquentielle mono-ligne dans le fichier texte, avec métadonnées simples, ID, Taille.
    - [ ] (Exercice 3) Lecture Du fichier texte en accès directe et stockage des méta-données trouvées dans une Liste + Lecture d'un badge correspondant aux méta-données
 - [ ] (Exercice 4) Permettre au wallet de recevoir plusieurs badges, et de les restituer sous forme de liste
    - [ ] Lecture Du fichier texte en accès directe et stockage des méta-données trouvées dans un Set, donc... 
    - [ ] Ajout d'une fonction de hachage dans les métadonnées 
    - [ ] Ajout d'une gestion d'exception lors de l'ajout de badge pour garantir l'unicité des badges.


## Consignes de l'Exercice 1

Commençons donc par l'exercice 1 de cette Séance, avec l'homogénéisation du projet.

### Homogénéisation
    
Pour ce faire, respecter les étapes suivantes:

 - [ ] Déplacer toutes les sources contenues dans le répertoire src vers l'arborescence du sous-projet "**badges-wallet**". Attention, il faudra veiller, au cas par cas, à choisir le bon répertoire, qu'il s'agisse de sources ou bien re ressources, applicatives ou bien de test.
 - [ ] Supprimer les répertoires lib et src, ainsi que leur contenu
 - [ ] Déplacer enfin toute ressources présente à la racine du projet dans l'arborescence du sous-projet, et dans le repertoire qui s'y prête le mieux (de test probablement).
 - [ ] S'assurer que tous les tests sont en **JUnit**, ainsi que les **assertions**.  
 - [ ] Tester de nouveau le build ainsi que l'exécution de l'ensemble des tests en une opération sur l'IDE (Eclipse ou autre choix), puis apporter la preuve que tout est "vert" avec une saisie d'écran.


   - ![homogénéisation][homogénéisation]

[homogénéisation]: ../../../../screenshots/homogénéisation.png "homogénéisation"
